// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

// Uncomment this line to use console.log
// import "hardhat/console.sol";

contract Momentum {
  uint public constant nextCount = 1;
  address payable public owner;
  string name;

  constructor(string memory _name) payable {
    name = _name;
    owner = payable(msg.sender);
  }
}
