FILE=$1
solc-select install 0.8.17
solc-select use 0.8.17
slither ${FILE} --ignore-compile --filter-paths "node_modules" --disable-color --print human-summary > slither-audit.txt 2>&1

export LError=$(grep 'low issues' slither-audit.txt | sed 's/[a-zA-Z:0 ]//g')
export MError=$(grep 'medium issues' slither-audit.txt | sed 's/[a-zA-Z:0 ]//g')
export HError=$(grep 'high issues' slither-audit.txt | sed 's/[a-zA-Z:0 ]//g')

echo -en '-------------------------------\n\n\n# Details:\n##############################\n\n' > slither-audit-full.txt
slither ${FILE} --ignore-compile --filter-paths "node_modules" --disable-color > slither-audit-full.txt 2>&1

LError=${LError:-"0"};
MError=${MError:-"0"};
HError=${HError:-"0"};

if [ ! -z "${LError}" ] || [ ! -z "${MError}" ] || [ ! -z "${HError}" ] ; then
  echo "Found ${LError} LOW ${MError} MIDDLE ${HError} HIGH Issues"
  echo 1
else
  echo 0
fi
