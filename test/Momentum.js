const {
  time,
  loadFixture,
} = require("@nomicfoundation/hardhat-toolbox/network-helpers");
const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");
const { expect } = require("chai");

describe("Momentum", function () {
  // We define a fixture to reuse the same setup in every test.
  // We use loadFixture to run this setup once, snapshot that state,
  // and reset Hardhat Network to that snapshot in every test.
  async function deployTokenFixture() {
    // Contracts are deployed using the first signer/account by default
    const [owner, otherAccount] = await ethers.getSigners();

    const ONE_GWEI = 1_000_000_000;
    const lockedAmount = ONE_GWEI;

    const NAME = "Momentum";
    const Momentum = await ethers.getContractFactory(NAME);
    const token = await Momentum.deploy(NAME, { value: lockedAmount });

    return { token, NAME, lockedAmount, owner, otherAccount };
  }

  describe("Deployment", function () {
    it("Should set the right nextCount", async function () {
      const { token, name } = await loadFixture(deployTokenFixture);

      expect(await token.nextCount()).to.equal(1);
    });
  });

});
